import { ErrorService } from './errorService'
import { TokenService } from './tokenService'
import { UtilService } from './utilService'
import { FirebaseService } from './firebaseService'
// Crud
import { ICrudExecOption, ICrudOption, CrudService } from './crudService'
// import { SampleCrudService } from './crud/sampleCrudService.mongo'
import { SampleCrudService } from './crud/sampleCrudService.pg'

const errorService = new ErrorService()
const tokenService = new TokenService()
const utilService = new UtilService()
const firebaseService = new FirebaseService()
// Crud
const sampleCrudService = new SampleCrudService()

export {
    errorService,
    tokenService,
    utilService,
    firebaseService,

    CrudService,
    ICrudExecOption,
    ICrudOption,
    sampleCrudService,

}