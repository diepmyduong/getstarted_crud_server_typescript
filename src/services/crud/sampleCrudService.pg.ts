import { CrudService } from '../crudService.pg'
import { SampleModel } from '@/models/tables'
export class SampleCrudService extends CrudService<typeof SampleModel> {
    constructor() {
        super(SampleModel)
    }

}