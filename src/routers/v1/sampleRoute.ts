import { CrudRouter } from '../crud'
import { Request, Response } from '../base'
import { sampleCrudController } from '@/controllers'
import { blockMiddleware, queryMiddleware } from '@/middlewares'

export default class SampleRouter extends CrudRouter<typeof sampleCrudController> {
    constructor() {
        super(sampleCrudController)
    }
    getListMiddlewares(): any[] {
        return [queryMiddleware.run()]
    }
    getItemMiddlewares(): any[] {
        return [queryMiddleware.run()]
    }
    updateMiddlewares(): any[] {
        return [blockMiddleware.run()]
    }
    deleteMiddlewares(): any[] {
        return [blockMiddleware.run()]
    }
    deleteAllMiddlewares(): any[] {
        return [blockMiddleware.run()]
    }
    createMiddlewares(): any[] {
        return [blockMiddleware.run()]
    }
}