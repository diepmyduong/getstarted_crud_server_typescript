import { CrudController } from '../crudController'
import { sampleCrudService, ICrudOption, errorService } from '@/services'


export class SampleCrudController extends CrudController<typeof sampleCrudService> {
    constructor() {
        super(sampleCrudService)
    }
}