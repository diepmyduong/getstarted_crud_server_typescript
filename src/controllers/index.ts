import { CrudController } from './crudController'
import { AuthController } from './authController'
import { SampleCrudController } from './crud/sampleCrudController'

const authController = new AuthController()

// Crud
const sampleCrudController = new SampleCrudController()

export {
    CrudController,
    authController,
    sampleCrudController,
}